#include <string>
#include <iostream>
#include "Character.h"
#include "Weapon.h"

using namespace std;

int main()
{
	string name1;
	string name2;
	int hp1;
	int hp2;
	int mp1;
	int mp2;


	cout << "Name of Player 1: "; cin >> name1; 
	cout << "HP: "; cin >> hp1;
	cout << "MP: "; cin >> mp1;

	cout << "Name of Player 2: "; cin >> name2;
	cout << "HP: "; cin >> hp2;
	cout << "MP: "; cin >> mp2;

	Character *player1 = new Character(name1, hp1, mp1);
	Character *player2 = new Character(name2, hp2, mp2);
	
	return 0;
}