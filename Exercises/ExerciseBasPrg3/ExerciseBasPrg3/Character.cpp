#include "Character.h"
#include <string>
#include "Weapon.h"
#include "Skill.h"
#include "NormalSkill.h"
#include "Vengeance.h"
#include "DopelBlade.h"
#include "CriticalSkill.h"
#include "Syphon.h"



Character::Character(string name, int hp, int mp)
{
	cName = name;
	cHp = hp;
	cMp = mp;
	currWeapon = new Weapon("Long Sword", 10);
	skills.push_back(new Vengeance());
	skills.push_back(new NormalSkill());
	skills.push_back(new DopelBlade());
	skills.push_back(new CriticalSkill());
	skills.push_back(new Syphon());
}

Character::~Character()
{
}

void Character::attack(Character * target)
{
}
