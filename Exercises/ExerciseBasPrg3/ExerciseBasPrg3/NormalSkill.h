#pragma once
#include <iostream>
#include "Skill.h"

class NormalSkill: public Skill
{
public:
	NormalSkill();
	~NormalSkill();
	void cast(Character* caster, Character *target);
};

