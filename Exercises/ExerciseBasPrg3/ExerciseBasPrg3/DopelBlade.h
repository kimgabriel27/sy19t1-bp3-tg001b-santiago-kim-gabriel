#pragma once
#include <iostream>
#include "Skill.h"

class DopelBlade: public Skill
{
public:
	DopelBlade();
	~DopelBlade();
	void cast(Character* caster, Character *target);
};

