#pragma once
#include <iostream>
#include "Skill.h"

class Vengeance:public Skill
{
public:
	Vengeance();
	~Vengeance();
	void cast(Character* caster, Character *target);
};

