#pragma once
#include <string>
#include "Weapon.h"
#include "Skill.h"
#include <vector>

using namespace std; 

class Skill;
class Character
{
public:
	Character(string name, int hp, int mp);
	~Character();
	void attack(Character *target);
	void cast();

	string getName();
	int getHp();
	int getMp();
	int getDamage();
	string getWName();

private:
	string cName;
	int cHp;
	int cMp;
	int cDamage;
	Weapon *currWeapon;
	vector<Skill*>skills;
};

