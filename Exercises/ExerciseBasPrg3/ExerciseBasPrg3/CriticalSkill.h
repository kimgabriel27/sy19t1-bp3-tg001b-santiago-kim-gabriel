#pragma once
#include <iostream>
#include "Skill.h"

class CriticalSkill: public Skill
{
public:
	CriticalSkill();
	~CriticalSkill();
	void cast(Character* caster, Character *target);
};

