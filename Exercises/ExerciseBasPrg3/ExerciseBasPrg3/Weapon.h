#pragma once
#include <string>
#include <iostream>

using namespace std; 

class Weapon
{
public:
	Weapon(string name, int damage);
	~Weapon();
	int getDamage();
	string getName();

private:
	string wName;
	int wDamage;
};

