#pragma once
#include "DopelBlade.h"
#include "Syphon.h"
#include "NormalSkill.h"
#include "CriticalSkill.h"
#include "Vengeance.h"

class Character;
class Skill
{
public:
	Skill(int mpCost, string name);
	~Skill();
	virtual void cast(Character* casterm, Character *target);
	
protected:
	int sMpCost;
	string sName;
};

