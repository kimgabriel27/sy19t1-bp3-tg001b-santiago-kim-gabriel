/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.cpp
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
      |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/

#include "TutorialApplication.h"
#include "OgreManualObject.h"

using namespace Ogre;


//---------------------------------------------------------------------------
TutorialApplication::TutorialApplication(void)
{
}
//---------------------------------------------------------------------------
TutorialApplication::~TutorialApplication(void)
{
}

//---------------------------------------------------------------------------
void TutorialApplication::createScene(void)
{
    // Create your scene here :)
	ManualObject *object = mSceneMgr->createManualObject();
	//Draw your Object Here 

	object->begin("BaseWhiteNoLighting", RenderOperation::OT_TRIANGLE_LIST);

	//Draw your Object Here 

	//Front Side
	object->position(10 / 2, 10 / 2, 10 / 2);
	object->position(-10 / 2, 10 / 2, 10 / 2);
	object->position(-10 / 2, -10 / 2, 10 / 2);

	object->position(10 / 2, 10 / 2, 10 / 2);
	object->position(-10 / 2, -10 / 2, 10 / 2);
	object->position(10 / 2, -10 / 2, 10 / 2);



	//Back Side
	object->position(-10 / 2, 10 / 2, -10 / 2);
	object->position(10 / 2, 10 / 2, -10 / 2);
	object->position(-10 / 2, -10 / 2, -10 / 2);


	object->position(10 / 2, 10 / 2, -10 / 2);
	object->position(10 / 2, -10 / 2, -10 / 2);
	object->position(-10 / 2, -10 / 2, -10 / 2);



	//Top Side
	object->position(-10 / 2, 10 / 2, -10 / 2);
	object->position(-10 / 2, 10 / 2, 10 / 2);
	object->position(10 / 2, 10 / 2, 10 / 2);

	object->position(-10 / 2, 10 / 2, -10 / 2);
	object->position(10 / 2, 10 / 2, 10 / 2);
	object->position(10 / 2, 10 / 2, -10 / 2);


	//RightSide
	object->position(10 / 2, 10 / 2, 10 / 2);
	object->position(10 / 2, -10 / 2, 10 / 2);
	object->position(10 / 2, -10 / 2, -10 / 2);


	object->position(10 / 2, 10 / 2, 10 / 2);
	object->position(10 / 2, -10 / 2, -10 / 2);
	object->position(10 / 2, 10 / 2, -10 / 2);

	//Left Side
	object->position(-10 / 2, 10 / 2, 10 / 2);
	object->position(-10 / 2, -10 / 2, -10 / 2);
	object->position(-10 / 2, -10 / 2, 10 / 2);


	object->position(-10 / 2, 10 / 2, 10 / 2);
	object->position(-10 / 2, 10 / 2, -10 / 2);
	object->position(-10 / 2, -10 / 2, -10 / 2);


	//Bottom Side
	object->position(-10 / 2, -10 / 2, 10 / 2);
	object->position(-10 / 2, -10 / 2, -10 / 2);
	object->position(10 / 2, -10 / 2, -10 / 2);


	object->position(-10 / 2, -10 / 2, 10 / 2);
	object->position(10 / 2, -10 / 2, -10 / 2);
	object->position(10 / 2, -10 / 2, 10 / 2);

	//End Drawing
	object->end();

	//Add the manual Oject to the Scene
	cubeNode = mSceneMgr->getRootSceneNode()->createChildSceneNode();
	cubeNode->attachObject(object);

	moveSpeed = 10;
}
//---------------------------------------------------------------------------

bool TutorialApplication::keyPressed(const OIS::KeyEvent &arg)
{
	IsMoving = true;
	return true;
}

bool TutorialApplication::keyReleased(const OIS::KeyEvent &arg)
{
	IsMoving = false;
	return true;
}

bool TutorialApplication::frameStarted(const FrameEvent &evt)
{
	if (mKeyboard->isKeyDown(OIS::KC_L))
	{
		cubeNode->translate(moveSpeed * evt.timeSinceLastFrame, 0, 0);
	}
	if (mKeyboard->isKeyDown(OIS::KC_J))
	{
		cubeNode->translate(-moveSpeed * evt.timeSinceLastFrame, 0, 0);

	}
	if (mKeyboard->isKeyDown(OIS::KC_I))
	{
		cubeNode->translate(0, 0, moveSpeed * evt.timeSinceLastFrame);
	}
	if (mKeyboard->isKeyDown(OIS::KC_K))
	{
		cubeNode->translate(0, 0, -moveSpeed * evt.timeSinceLastFrame);
	}
	if (mKeyboard->isKeyDown(OIS::KC_NUMPAD4))
	{

		Degree rotation = Degree(30 * evt.timeSinceLastFrame);
		cubeNode->rotate(Vector3(0, -1, 0), Radian(rotation));
	}
	if (mKeyboard->isKeyDown(OIS::KC_NUMPAD6))
	{

		Degree rotation = Degree(30 * evt.timeSinceLastFrame);
		cubeNode->rotate(Vector3(0, 1, 0), Radian(rotation));
	}
	if (mKeyboard->isKeyDown(OIS::KC_NUMPAD8))
	{

		Degree rotation = Degree(30 * evt.timeSinceLastFrame);
		cubeNode->rotate(Vector3(-1, 0, 0), Radian(rotation));
	}
	if (mKeyboard->isKeyDown(OIS::KC_NUMPAD2))
	{

		Degree rotation = Degree(30 * evt.timeSinceLastFrame);
		cubeNode->rotate(Vector3(1, 0, 0), Radian(rotation));
	}


	if (!IsMoving)
	{
		moveSpeed = 10;
	}
	else
	{
		moveSpeed += 10 * evt.timeSinceLastFrame;
	}
	return true;
}
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
    INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
    int main(int argc, char *argv[])
#endif
    {
        // Create application object
        TutorialApplication app;

        try {
            app.go();
        } catch(Ogre::Exception& e)  {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
            MessageBox(NULL, e.getFullDescription().c_str(), "An exception has occurred!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
            std::cerr << "An exception has occurred: " <<
                e.getFullDescription().c_str() << std::endl;
#endif
        }

        return 0;
    }

#ifdef __cplusplus
}
#endif

//---------------------------------------------------------------------------
