#include "Planet.h"
#include "TutorialApplication.h"
#include <math.h>

using namespace Ogre;


Planet::Planet(SceneNode * node)
{
	mNode = node;
	
}

Planet * Planet::createPlanet(SceneManager *sceneManager, float size, ColourValue colour)
{
	ManualObject *planet = sceneManager->createManualObject();

	//begin Drawing
	planet->begin("BaseWhiteNoLighting", RenderOperation::OT_TRIANGLE_LIST);

	//Draw your Object Here 

	planet->colour(colour);
	//Front Side
	planet->position(size/2, size/2, size/2);
	planet->position(-size/2, size/2, size/2);
	planet->position(-size/2, -size/2, size/2);

	planet->position(size/2, size/2, size/2);
	planet->position(-size / 2, -size / 2, size / 2);
	planet->position(size / 2, -size / 2, size / 2);



	//Back Side
	planet->position(-size/2, size/2, -size/2);
	planet->position(size / 2, size / 2, -size/2);
	planet->position(-size / 2, -size / 2, -size / 2);


	planet->position(size/2, size/2, -size/2);
	planet->position(size/2, -size / 2, -size/2);
	planet->position(-size / 2, -size/2, -size/2);



	//Top Side
	planet->position(-size/2, size/2, -size/2);
	planet->position(-size/2, size/2, size/2);
	planet->position(size/2, size/2, size/2);

	planet->position(-size / 2, size / 2, -size / 2);
	planet->position(size / 2, size / 2, size / 2);
	planet->position(size/2, size/2, -size/2);


	//RightSide
	planet->position(size / 2, size / 2, size / 2);
	planet->position(size/2, -size/2, size/2);
	planet->position(size/2, -size/2, -size/2);


	planet->position(size / 2, size / 2, size / 2);
	planet->position(size/2, -size/2, -size/2);
	planet->position(size/2, size/2, -size/2);

	//Left Side
	planet->position(-size/2, size/2, size/2);
	planet->position(-size/2, -size/2, -size/2);
	planet->position(-size/2, -size/2, size/2);


	planet->position(-size / 2, size / 2, size / 2);
	planet->position(-size/2, size/2, -size/2);
	planet->position(-size/2, -size/2, -size/2);


	//Bottom Side
	planet->position(-size / 2, -size/2 , size/2);
	planet->position(-size / 2, -size / 2, -size / 2);
	planet->position(size/2, -size/2, -size/2);


	planet->position(-size / 2, -size / 2, size / 2);
	planet->position(size / 2, -size / 2, -size / 2);
	planet->position(size/2, -size/2, size/2);

	//End Drawing
	planet->end();

	SceneNode *createNode = sceneManager->getRootSceneNode()->createChildSceneNode();
	createNode->attachObject(planet);

	return new Planet(createNode);
}

Planet::~Planet()
{

}

void Planet::update(const FrameEvent & evt)
{
	//Rotation 
	Degree rotation = Degree( mLocalRotationSpeed * evt.timeSinceLastFrame);
	mNode->rotate(Vector3(0, 1, 0), Radian(rotation));

	//Revolution  https://www.gamefromscratch.com/post/2012/11/24/GameDev-math-recipes-Rotating-one-point-around-another-point.aspx
	float revolution = (mRevolutionSpeed * evt.timeSinceLastFrame) * (Math::PI/180);
	float oldX = mNode->getPosition().x - mParent->getNode().getPosition().x;
	float oldZ = mNode->getPosition().z - mParent->getNode().getPosition().z;

	float newX = (oldX * (Math::Cos(Radian(revolution)))) + (oldZ * (Math::Sin(Radian(revolution))));
	float newZ = (oldX * -(Math::Sin(Radian(revolution)))) + (oldZ * (Math::Cos(Radian(revolution))));
	mNode->setPosition(Vector3(newX, mNode->getPosition().y, newZ));
}

SceneNode & Planet::getNode()
{
	// TODO: insert return statement here

	return *mNode;
}

void Planet::setParent(Planet * parent)
{
	mParent = parent;
}

Planet * Planet::getParent()
{	

	return mParent;
}
  
void Planet::setLocalRotationSpeed(float speed)
{
	mLocalRotationSpeed = speed;
}
	
void Planet::setRevolutionSpeed(float speed)
{
	if (speed == 0)
	{
		mRevolutionSpeed = speed;
	}
	else
	{
		mRevolutionSpeed = (365 / speed) * 60;
	}

}


