#pragma once
#include "Item.h"
#include <string>
#include <iostream>

using namespace std;

class Potion :
	public Item
{
public:
	Potion(string name, int crystalCost, int pullCount);
	~Potion();
	void obtain(Player *player);

	int getCount();
	string getName();
};

