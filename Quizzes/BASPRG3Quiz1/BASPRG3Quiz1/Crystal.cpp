#include "Crystal.h"
#include "Player.h"
#include <string>
#include <iostream>

using namespace std;


Crystal::Crystal(string name, int crystalCost, int pullCount): Item (name, crystalCost, pullCount)
{
}

Crystal::~Crystal()
{
}

void Crystal::obtain(Player * player)
{
	cout << "You Pull a Crystal." << endl;
	cout << "You gain 15 more Crystal. " << endl;
	mPullCount++;
	player->gainCrystal();
}

int Crystal::getCount()
{
	return mPullCount;
}

string Crystal::getName()
{
	return (mName);
}
