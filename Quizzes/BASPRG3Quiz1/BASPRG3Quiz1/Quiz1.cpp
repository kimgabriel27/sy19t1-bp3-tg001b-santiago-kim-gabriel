#include <iostream>
#include <string>
#include "Player.h"
#include "Item.h"
#include "SSR.h"
#include "SR.h"
#include "R.h"
#include "Potion.h"
#include "Item.h"
#include "Crystal.h"
#include "Bomb.h"
#include <time.h>

using namespace std;

int main()
{
	Player *player = new Player(100, 100, 0, 0);


	while (player->getHp() > 0 && player->getCrystal() > 0 && player->getRP() < 100)
	{
		srand(time(NULL));
		player->getStats();

		player->pullItem();
   		system("pause"); 
 		system("CLS");
	}
	
	cout << "=============================" << endl;

	player->getStats();

	cout << "=============================" << endl;

 	player->collateItems();
 	system("pause");

 	return 0;
}