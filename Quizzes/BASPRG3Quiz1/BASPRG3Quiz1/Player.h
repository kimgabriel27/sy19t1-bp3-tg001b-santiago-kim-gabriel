#pragma once
#include "Item.h"
#include <vector>

class Player
{
public:
	Player(int hp, int crystal, int rarityPoints, int pulls);
	~Player();
	void pullItem();
	
	void gainRP(int value);
	void gainHp();
	void gainCrystal();
	void loseHp();
	void loseCrystal();
	

	void collateItems();
	void getStats();
	int getHp();
	int getCrystal();
	int getRP();
	int getPulls();

private:
	int mHp;
	int mCrystal;
	int mRarityPoints;
	int mPulls; 
	vector<Item*>items;
};

