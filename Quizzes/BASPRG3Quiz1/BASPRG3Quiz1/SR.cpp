#include "SR.h"
#include "Player.h"
#include <string>
#include <iostream>

using namespace std;
SR::SR(string name, int crystalCost, int pullCount) :Item(name, crystalCost, pullCount)
{
}

SR::~SR()
{
}

void SR::obtain(Player * player)
{
	cout << "Great Pull!" << endl;
	cout << "You Pull a SR Item." << endl;
	cout << "You gain 10 Rarity Points " << endl;
	mPullCount++;
	player->gainRP(10);
}

int SR::getCount()
{
	return mPullCount;
}

string SR::getName()
{
	return (mName);
}

