#pragma once
#include "Item.h"
#include <string>
#include <iostream>

using namespace std;

class SR :
	public Item
{
public:
	SR(string name, int crystalCost, int pullCount);
	~SR();
	void obtain(Player *player);

	int getCount();
	string getName();
};

