#include "R.h"
#include "Player.h"
#include <string>
#include <iostream>

using namespace std;

R::R(string name, int crystalCost, int pullCount): Item(name,crystalCost,pullCount)
{
}

R::~R()
{
}

void R::obtain(Player * player)
{
	cout << "You Pull a R Item." << endl;
	cout << "You gain 1 Rarity Points " << endl;
 	mPullCount++;
	player->gainRP(1);
	
}

int R::getCount()
{
	return mPullCount;
}

string R::getName()
{
	return (mName);
}

