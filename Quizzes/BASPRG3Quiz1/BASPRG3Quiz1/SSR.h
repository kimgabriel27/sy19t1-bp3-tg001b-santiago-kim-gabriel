#pragma once
#include "Item.h"
#include <string>

using namespace std;

class SSR :
	public Item
{
public:
	SSR(string name, int crystalCost, int pullCount);
	~SSR();
	void obtain(Player *player);
	
	int getCount();
	string getName();
};

