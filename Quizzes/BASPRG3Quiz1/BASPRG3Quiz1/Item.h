#pragma once
#include <string>

using namespace std;

class Player;
class Item
{
public:
	Item(string name, int crystalCost, int pullCount);
	~Item();

	virtual void obtain(Player *player);
	

	virtual int getCount();
	virtual string getName();
protected:
	string mName;
	int mCrystalCost;
	int mPullCount;
};

