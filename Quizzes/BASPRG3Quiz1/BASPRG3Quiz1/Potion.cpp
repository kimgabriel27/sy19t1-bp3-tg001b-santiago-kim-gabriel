#include "Potion.h"
#include "Player.h"
#include <string>
#include <iostream>

using namespace std;

Potion::Potion(string name, int crystalCost, int pullCount): Item (name, crystalCost, pullCount)
{
}

Potion::~Potion()
{
}

void Potion::obtain(Player * player)
{
	cout << "You Pull a Health Potion." << endl;
	cout << "You heal 30 HP" << endl;
	mPullCount++;
	player->gainHp();
}

int Potion::getCount()
{
	return mPullCount;
}

string Potion::getName()
{
	return (mName);
}
