#pragma once
#include "Item.h"
#include <string>
#include <iostream>

using namespace std;

class Crystal :
	public Item
{
public:
	Crystal(string name, int crystalCost, int pullCount);
	~Crystal();
	void obtain(Player *player);

	int getCount();
	string getName();	

};

