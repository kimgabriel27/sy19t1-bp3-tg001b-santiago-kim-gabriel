#pragma once
#include "Item.h"
#include <string>
#include <iostream>

using namespace std;

class R :
	public Item
{
public:
	R(string name, int crystalCost, int pullCount);
	~R();
	void obtain(Player *player);

	int getCount();
	string getName();
};

