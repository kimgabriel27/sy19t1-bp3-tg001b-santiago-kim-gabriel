#include "Bomb.h"
#include "Player.h"
#include <string>
#include <iostream>

using namespace std;

Bomb::Bomb(string name, int crystalCost, int pullCount): Item(name,crystalCost, pullCount)
{
}

Bomb::~Bomb()
{
}

void Bomb::obtain(Player *player)
{
	cout << "OH NO!" << endl;
	cout << "You Pull a Bomb." << endl;
	cout << "You lose 25 HP. " << endl;
	mPullCount++;
	player->loseHp();
}

int Bomb::getCount()
{
	return mPullCount;
}

string Bomb::getName()
{
	return (mName);
}

