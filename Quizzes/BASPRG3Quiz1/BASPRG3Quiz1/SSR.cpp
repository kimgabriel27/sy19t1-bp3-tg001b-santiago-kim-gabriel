#include "SSR.h"
#include "Player.h"
#include <string>
#include <iostream>

using namespace std;

SSR::SSR(string name, int crystalCost, int pullCount): Item(name,crystalCost, pullCount)
{
}

SSR::~SSR()
{
}

void SSR::obtain(Player * player)
{
	cout << "LUCKY YOU!" << endl;
	cout << "You Pull a SSR Item." << endl;
	cout << "You gain 50 Rarity Points " << endl;
	mPullCount++;
	player->gainRP(50);
}

int SSR::getCount()
{
	return mPullCount;
}

string SSR::getName()
{
	return (mName);
}
