#pragma once
#include "Item.h"
#include <iostream>
#include <string>

using namespace std;

class Bomb :
	public Item
{
public:
	Bomb(string name, int crystalCost, int pullCount);
	~Bomb();
	void obtain(Player * player);

	int getCount();
	string getName();
};

