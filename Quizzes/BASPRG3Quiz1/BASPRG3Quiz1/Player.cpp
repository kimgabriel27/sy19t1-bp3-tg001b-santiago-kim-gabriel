#include "Player.h"
#include "SSR.h"
#include "SR.h"
#include "R.h"
#include "Potion.h"
#include "Crystal.h"	
#include "Bomb.h"
#include <iostream>

using namespace std;

Player::Player(int hp, int crystal, int rarityPoints, int pulls)
{
	mHp = hp;
	mCrystal = crystal;
	mRarityPoints = rarityPoints;
	mPulls = pulls;
	items.push_back(new SSR("SSR", 5,0));
	items.push_back(new SR("SR", 5, 0));
	items.push_back(new R("R", 5, 0));
	items.push_back(new Potion("Potion", 5, 0));
	items.push_back(new Bomb("Bomb", 5, 0));
	items.push_back(new Crystal("Crystal", 5, 0 ));

}

Player::~Player()
{
}

void Player::pullItem()
{
	int randChance = rand() % 99 + 1;
	if (randChance == 1)// pull SSR 
	{
		items[0]->obtain(this);
	}
	else if (randChance > 1 && randChance <= 10)//pull SR 
	{
		items[1]->obtain(this);
	}
	else if (randChance > 10 && randChance <= 50)//pull R
	{ 
		items[2]->obtain(this);
	}
	else if (randChance > 50 && randChance <= 65)//pull Health Potion
	{
		items[3]->obtain(this);
	}
	else if (randChance > 65 && randChance <= 85)//pull Bomb
	{
		items[4]->obtain(this);
	}
	else if (randChance > 85 && randChance <= 100)//pull Crystal
	{
		items[5]->obtain(this);
	}
	loseCrystal();
	mPulls++;
}

void Player::gainRP(int value)
{
	this->mRarityPoints += value;
}

void Player::gainHp()
{
	this->mHp += 30;
}

void Player::gainCrystal()
{
	this->mCrystal += 15;
}

void Player::loseHp()
{
	this->mHp -= 25;
}

void Player::loseCrystal()
{
	this->mCrystal -= 5;
}

void Player::collateItems()
{
	for (int i = 0; i < 6; i++)
	{
		if (items[i]->getCount() > 0)
		{
			cout << items[i]->getName() << " x" << items[i]->getCount() << endl;
		}
	}
}

void Player::getStats()
{
	cout << "HP: " << this->getHp() << endl;
	cout << "Crystal Remaining: " << this->getCrystal() << endl;
	cout << "Rarity Points: " << this->getRP() << endl;
	cout << "Pulls: " << this->getPulls() << endl << endl << endl;

}

int Player::getHp()
{
	return mHp;
}

int Player::getCrystal()
{
	return mCrystal;
}

int Player::getRP()
{
	return mRarityPoints;
}

int Player::getPulls()
{
	return mPulls;
}
