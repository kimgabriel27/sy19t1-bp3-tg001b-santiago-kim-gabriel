#pragma once
#include <string>
#include <iostream>
#include "Map.h"
#include "Weapon.h"

using namespace std;

class Monster;
class Armor;
class Weapon;
class ClassCreator;
class Map;  
class Player
{
	public:
	Player();
	~Player();
	string name;
	string className;
	int currHp;
	int maxHp;
	int pow;
	int vit;
	int agi;
	int dex;
	int hitRate;
	int pGold = 0;
	int x;
	int y;
	int level = 1;
	int currExp = 0;
	int requiredExp = (level * 1000) - currExp;
	Armor *currArmor;
	Weapon *currWeapon;
	Map *map;

	void chooseJob(int chosenNum, string chosenName);
	void equipWeapon(Weapon *currWeapon);
	void viewStats();
	void rest();
	void attackMonster(Monster *target);
	void lootMonster(Monster *monster);
	void changeWeapon();
	void changeArmor();
	void levelUp();
	};