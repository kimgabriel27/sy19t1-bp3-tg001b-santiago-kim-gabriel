#include "Player.h"
#include "Weapon.h"
#include "Monster.h"
#include <string>
#include <iostream>
#include <time.h>

using namespace std;

Player::Player()
{
}


Player::~Player()
{
}

void Player::chooseJob(int chosenNum, string chosenName)
{

	if (chosenNum == 1)//Knight Class
	{
		className = "Warrior";
		name = chosenName;
		maxHp = 100;
		pow = 15;
		vit = 3;
		agi = 10;
		dex = 9;
		currHp = maxHp;
		Weapon *startingWeapon = new Weapon("Broad Sword", 8, 0);
		currWeapon = startingWeapon;
	}
	else if (chosenNum == 2)//Thief Class
	{
		className = "Thief";
		name = chosenName;
		maxHp = 100;
		pow = 9;
		vit = 3;
		agi = 15;
		dex = 18;
		currHp = maxHp;
		Weapon *startingWeapon = new Weapon("Dagger", 4, 0);
		currWeapon = startingWeapon;
	}
	else if (chosenNum == 3)//Crusader Class
	{
		className = "Crusader";
		name = chosenName;
		maxHp = 150;
		pow = 5;
		vit = 7;
		agi = 8;
		dex = 9;
		currHp = maxHp;	
		Weapon *startingWeapon = new Weapon("Dagger", 4, 0);
		currWeapon = startingWeapon;
	}
}
	void Player::viewStats()
	{
		cout << "                     Your Stats " << endl;
		cout << "====================================================" << endl;
		cout << "Player Name: " << name << endl;
		cout << "HP: " << currHp << " / " << maxHp << endl;
		cout << "Class: " << className << endl;
		cout << "POW: " << pow << endl;
		cout << "VIT: " << vit << endl;
		cout << "AGI: " << agi << endl;
		cout << "DEX: " << dex << endl;
		cout << "Weapon: " << currWeapon->wName << " || Damage: " << currWeapon->wDamage << endl;
		cout << "Current Level: " << level << endl;
		cout << "Current Exp: " << currExp << endl;
		cout << "Required Exp to Level: " << (level * 1000) - currExp << endl;
		cout << "====================================================" << endl;
		/*cout << "Weapon: " << currWeapon->name << " | Damage : " << pPow + currWeapon->damage << endl;*/

		system("pause");
	}

	void Player::rest()
	{
		cout << "You Rest......" << endl;
		cout << "You regain Health " << endl;
		currHp = maxHp;
		system("pause");
	}

	void Player::attackMonster(Monster * target)
	{
		srand(time(NULL));
		int hitRate = (dex / target->agi) * 100;
		if (hitRate < 20)
		{
			hitRate = 20;
		}
		else if (hitRate > 80)
		{
			hitRate = 80;
		}
		int randAttack = rand() % 100 + 1;

		if (randAttack <= hitRate)
		{
			int playerDamage = (pow + currWeapon->wDamage) - target->vit;
			/*if (playerDamage <= 0)
			{
				playerDamage = 0;
			}
			else 
			{
				playerDamage = (pow + currWeapon->wDamage) - target->vit;
			}*/
			cout << this->name << " Attack " << target->name << endl;
			cout << this->name << " Deals " << playerDamage << " damage." << endl;
			target->hp -= playerDamage;
		}
		else 
		{
			cout << this->name << " Miss the Attack." << endl;
		}
	
	}

	void Player::lootMonster(Monster *monster)
	{
		int expLoot;
		int goldLoot;
		if (monster->name == "Goblin")
		{
			expLoot = 100;
			goldLoot = 10;
			cout << "You gain " << endl; 
			cout << expLoot << " exp" << endl;
			cout << goldLoot << " gold" << endl;
		}
		else if (monster->name == "Ogre")
		{
			expLoot = 250;
			goldLoot = 50;
			cout << "You gain " << endl;
			cout << expLoot << " exp" << endl;
			cout << goldLoot << " gold" << endl;
		}
		else if (monster->name == "Orc")
		{
			expLoot = 500;
			goldLoot = 100;
			cout << "You gain " << endl;
			cout << expLoot << " exp" << endl;
			cout << goldLoot << " gold" << endl;
		}
		else if (monster->name == "Orc Lord")
		{
			expLoot = 1000;
			goldLoot = 100;
			cout << "You gain " << endl;
			cout << expLoot << " exp" << endl;
			cout << goldLoot << " gold" << endl;
		}
		system("pause");
		currExp +=  expLoot;
		pGold += goldLoot;
	}

	void Player::levelUp()
	{
		srand(time(NULL));
		level += 1;
		int randPow = rand() % 6;
		int randAgi = rand() % 6;
		int randDex = rand() % 6;
		int randVit = rand() % 6;
		int randHp = rand() % 10 + 5;

		cout << "You reach level " << level << endl;
		cout << "Stats Gained:" << endl;
		cout << "Max HP +" << randHp << endl;
		cout << "Pow + " << randPow << endl;
		cout << "Vit + " << randVit << endl;
		cout << "Dex + " << randDex << endl;
		cout << "Agi + " << randAgi << endl;
		
		pow += randPow;
		vit += randVit;
		dex += randDex;
		agi += randAgi;
		maxHp += randHp;
	}

	
