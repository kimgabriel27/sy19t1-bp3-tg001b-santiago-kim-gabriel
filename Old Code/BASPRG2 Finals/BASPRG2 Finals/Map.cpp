#include "Map.h"
#include "Player.h"
#include "Monster.h"
#include <time.h>
#include <iostream>

using namespace std;
Map::Map(Player * mainPlayer)
{
	mainPlayer->x = x;
	mainPlayer->y = y;
}

Map::~Map()
{
}

void Map::displayPosition()
{
	cout << "Your Position ( " << x << " , " << y << " ) " << endl;
}

void Map::checkCoordinates(Player * mainPlayer)
{
	srand(time(NULL));
	int chanceEncounter = (rand() % 100) + 1;
	if (chanceEncounter >= 0 && chanceEncounter <= 20)
	{
		cout << "Nothing Encounter" << endl;

	}
	else if (chanceEncounter >= 21 && chanceEncounter <= 45)
	{
		Monster *monster = new Monster("Goblin", 40, 8, 5, 8, 8);
		battleMonster(monster, mainPlayer);
	}
	else if (chanceEncounter >= 46 && chanceEncounter <= 70)
	{
		Monster *monster = new Monster("Ogre", 80, 12, 10, 5, 8);
		battleMonster(monster, mainPlayer);
	}
	else if (chanceEncounter >= 71 && chanceEncounter <= 95)
	{
		Monster *monster = new Monster("Orc", 120, 15, 13, 8, 8);
		battleMonster(monster, mainPlayer);
	}
	else if (chanceEncounter >= 96 && chanceEncounter <= 100)
	{
		Monster *monster = new Monster("Orc Lord", 1000, 30, 50, 15, 15);
		battleMonster(monster, mainPlayer);
	}
}

void Map::battleMonster(Monster * monster, Player * mainPlayer)
{
	srand(time(NULL));
	cout << "You've Encounter a Monster!" << endl;
	monster->viewStats();
	while (monster->hp > 0 && mainPlayer->currHp > 0)
	{
		int chosenAction = 0;
		cout << mainPlayer->name << " HP: " << mainPlayer->currHp << "/" << mainPlayer->maxHp << "      " << monster->name << " HP: " << monster->hp << endl << endl;
		cout << "What will be your Action?" << endl;
		cout << "[1] Attack " <<" [2] Run Away" << endl;
		cout << "Your Action: "; cin >> chosenAction;
		system("CLS");
		if (chosenAction == 1)
		{
			mainPlayer->attackMonster(monster);
			cout << endl;
			system("pause");
			monster->attackPlayer(mainPlayer);
			cout << endl;
			system("pause");
		}
		else
		{
			int runChance = rand() % 100 + 1;
			if (runChance <= 25)
			{
				cout << "You've Run Away from the Battle." << endl;
				system("pause");
				break;
			}
			else
			{
				cout << "You've failed to Run Away from the Battle." << endl;
				system("pause");
				monster->attackPlayer(mainPlayer);
			}
		}
	}
	if (monster->hp <= 0)
	{
		cout << "You defeated the Monster." << endl;
		mainPlayer->lootMonster(monster);
		delete monster;
	}

}

void Map::checkLevel(Player * mainPlayer)
{
	if (mainPlayer->requiredExp <= 0)
	{
		mainPlayer->levelUp();
	}
}

void Map::movePlayer(Player * player)
{
	int choseMove;
	cout << " [1]North  [2]South  [3]East  [4]West " << endl;
	cout << "Direction: ";  cin >> choseMove;

	if (choseMove == 1)
	{
		y++;
		checkCoordinates(player);
	}
	else if (choseMove == 2)
	{
		y--;
		checkCoordinates(player);
	}
	else if (choseMove == 3)
	{
		x++;
		checkCoordinates(player);
	}
	else if (choseMove == 4)
	{
		x--;
		checkCoordinates(player);
	}
}
