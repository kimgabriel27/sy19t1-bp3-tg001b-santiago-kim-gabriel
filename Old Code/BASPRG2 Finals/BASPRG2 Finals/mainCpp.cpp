#include <iostream>
#include <string>
#include "Player.h"
#include "Monster.h"
#include "Map.h"

using namespace std;

int main()
{
	int numClass;
	string name;
	cout << "Input Character Name: ";
	cin >> name;

	cout << "Choose your Class " << endl;
	cout << "[1] Knight - High POW & Better Weapon" << endl;
	cout << "[2] Thief - High DEX & AGI" << endl;
	cout << "[3] Crusader - HIGH HP & VIT" << endl;
	cout << "Chosen Class: "; cin >> numClass;
	
	Player *mainPlayer = new Player();
	mainPlayer->chooseJob(numClass, name);
	Map *map = new Map(mainPlayer);
	system("CLS");
	while (mainPlayer->currHp != 0)
	{
		map->displayPosition();
		int choiceAction = 0;
		cout << "[1] Move " << " [2] Rest " << " [3] View Stats " << " [4] Quit" << endl;
		cout << "Action: "; cin >> choiceAction;
		system("CLS");
		if (choiceAction == 1)
		{
			map->movePlayer(mainPlayer);
		}
		else if (choiceAction == 2)
		{
			mainPlayer->rest();
		}
		else if (choiceAction == 3)
		{
			mainPlayer->viewStats();
		}
		else if (choiceAction == 4)
		{
			cout << "Goodbye, See you Again.";
			delete mainPlayer;
			delete map;
			break;
		}

	}
}