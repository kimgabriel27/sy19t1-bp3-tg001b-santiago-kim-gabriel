#pragma once
#include <iostream>

using namespace std;

class Weapon
{
public:
	Weapon(string name, int damage, int goldCost);
	~Weapon();
	string wName;
	int wDamage;
	int wGoldCost;
};

