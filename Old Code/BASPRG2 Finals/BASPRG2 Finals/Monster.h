#pragma once
#include <string>
#include <iostream>
using namespace std;
class Player;
class Monster
{
public:
	~Monster();
	Monster(string mName, int mHp, int mPow, int mVit, int mAgi, int dex);
	string name;
	int hp;
	int pow;
	int vit;
	int agi;
	int dex;

	void attackPlayer(Player *target);
	void viewStats();
};

