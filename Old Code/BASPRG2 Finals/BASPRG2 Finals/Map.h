#pragma once
#include <iostream>

using namespace std;
class Player;
class Monster;
class Map
{
public:
	Map(Player *mainPlayer);
	~Map();
	int x = 0;
	int y = 0;

	void displayPosition();
	void movePlayer(Player *player);
	void checkCoordinates(Player* mainPlayer);
	void battleMonster(Monster *monster, Player *mainPlayer);
	void checkLevel(Player *mainPlayer);
};

