#include "Monster.h"
#include "Player.h"
#include <time.h>
#include <iostream>
#include <string>

using namespace std;

Monster::~Monster()
{
}

Monster::Monster(string mName, int mHp, int mPow, int mVit, int mAgi, int mDex)
{
	name = mName;
	hp = mHp;
	pow = mPow;
	vit = mVit;
	agi = mAgi;
	dex = mDex;
}

void Monster::attackPlayer(Player * target)
{
	srand(time(NULL));
	int hitRate = (dex / target->agi) * 100;
	if (hitRate < 20)
	{
		hitRate = 20;
	}
	else if (hitRate > 80)
	{
		hitRate = 80;
	}
	int randAttack = rand() % 100 + 1;

	if (randAttack <= hitRate)
	{
		int monsterDamage = pow - target->vit;
		cout << this->name << " Attack " << target->name << endl;
		cout << this->name << " Deals " << monsterDamage << " damage." << endl;
		target->currHp -= monsterDamage;
	}
	else
	{
		cout << this->name << " Miss the Attack." << endl;
	}

}

void Monster::viewStats()
{
	cout << "  Monster Stats" << endl;
	cout << "==================" << endl;
	cout << "Name: " << name << endl;
	cout << "HP: " << hp << endl;
	cout << "POW: " << pow << endl;
	cout << "VIT: " << vit << endl;
	cout << "AGI: " << agi << endl;
	cout << "DEX: " << dex << endl;
	system("Pause");
}
