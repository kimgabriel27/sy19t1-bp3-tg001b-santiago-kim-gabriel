#include <string>
#include <iostream>
#include <vector>
#include <algorithm>
#include <time.h>
#include "Unit.h"
#include "Skill.h"


using namespace std;


void printTeamStats(vector<Unit*>team)
{
	for (int i = 0; i < team.size(); i++)
	{
		cout << team[i]->getName() << " [HP: " << team[i]->getHp() << " ]" << endl;
	}
}

void BattleMenu(vector<Unit*>&team1, vector<Unit*>&team2 ,vector<Unit*>&allUnits)
{
	cout << "====================================" << endl;
	cout << "Team: Player" << endl;
	cout << "====================================" << endl;
	
	printTeamStats(team1);

	cout << "====================================" << endl;
	cout << "Team: Enemy" << endl;
	cout << "====================================" << endl;

	printTeamStats(team2);

}

void groupTeam(vector<Unit*>&team, Unit* unit1, Unit* unit2, Unit *unit3)
{
	team.push_back(unit1);
	team.push_back(unit2);
	team.push_back(unit3);
}

void sortOrder(vector<Unit*>&allUnits, Unit* unit1, Unit* unit2, Unit *unit3, Unit *unit4, Unit *unit5, Unit * unit6)
{
	allUnits.push_back(unit1);
	allUnits.push_back(unit2);
	allUnits.push_back(unit3);
	allUnits.push_back(unit4);
	allUnits.push_back(unit5);
	allUnits.push_back(unit6);


	for (int i = 0; i < 6; i++)
	{
		for (int k = 0; k < 6; k++)
		{
			if (allUnits[k]->getAgi() < allUnits[i]->getAgi())
			{
				swap(allUnits[k],allUnits[i]);
			}
		}
	}

}

void checkHp(vector<Unit*>&group , vector<Unit*>&allUnits)
{
	int checkPos = 0;
	for (int i = 0; i < group.size(); i++)
	{
		if (group[i]->getHp() <= 0)
		{
			group.erase(group.begin() + i);
		}
	}
	for (int i = 0; i < allUnits.size(); i++)
	{
		if (allUnits[i]->getHp() <= 0)
		{
			allUnits.erase(allUnits.begin() + i);
		}
	}
}

void printOrder(vector<Unit*>&allUnits, int turnNum )
{
	cout << "===============TURN ORDER================" << endl;
	for (int i = 0; i < allUnits.size(); i++)
	{
		cout <<"[" << allUnits[i]->getTeam() << "]   " << allUnits[i]->getName() << endl;
	}
	cout << "===============TURN ORDER================" << endl << endl;
	
	cout << "Current Turn: [" << allUnits[turnNum]->getTeam() << "] " << allUnits[turnNum]->getName() << endl;
	
}

int main()
{
	srand(time(NULL));
	vector<Unit*>allUnits;
	vector<Unit*>playerTeam;
	vector<Unit*>enemyTeam;

	int round = 0;
	int turnNum;

	
	Unit *p1 = new Unit("Chris", "Warrior","Player", 200, 80, 100, 10, 7, 9);
	Unit *p2 = new Unit("Javalla", "Mage", "Player",80,250, 50,6,7,8);
	Unit *p3 = new Unit("Nocturne", "Assasin","Player", 90, 50, 60,9,5,5);
	groupTeam(playerTeam, p1, p2, p3); //Player Team

	Unit *e1 = new Unit("Garen", "Warrior", "AI",250, 80, 9, 8, 7, 9);
	Unit *e2 = new Unit("Veigar", "Mage", "AI", 75, 250, 6, 7, 7, 8);
	Unit *e3 = new Unit("Kha'zix", "Assasin", "AI", 80, 50, 10, 4, 5, 5);

	groupTeam(enemyTeam, e1, e2, e3); //Enemy Team
	
	sortOrder(allUnits, p1, p2, p3, e1, e2, e3); //sort ORder according to Agi



	while (playerTeam.size() > 0 && enemyTeam.size() > 0)
	{
		turnNum = round % allUnits.size();
		BattleMenu(playerTeam, enemyTeam, allUnits);

		printOrder(allUnits, turnNum);	

		system("pause");
		system("CLS");

		if (allUnits[turnNum]->getTeam() == "AI") //If Enemy AI, random choose of action/Skill
		{
			allUnits[turnNum]->randomAction(playerTeam,enemyTeam);
		}	
		else
		{
			allUnits[turnNum]->chooseAction(playerTeam,enemyTeam);// Player will choose what will be his/her move
		}

		checkHp(playerTeam, allUnits);
		checkHp(enemyTeam, allUnits);
		
		system("CLS");

		round++;
	}

	if (playerTeam.size() == 0)
	{
		cout << "All of your Champions are Dead!!" << endl;
		cout << "You have lost the Battle!!" << endl;
	}

	else if (enemyTeam.size() == 0)
	{
		cout << "You've killed all of the enemy's Champions!!" << endl;
		cout << "Tou have won the Battle!!" << endl;
	}

	system("pause");

	delete p1;
	delete p2;
	delete p3;
	delete e1;
	delete e2;
	delete e3;

	return 0;
}
