#pragma once
#include <string>
#include <iostream>
#include "Unit.h"
using namespace std; 


class Skill
{
public:
	Skill(string name, int mpCost);
	~Skill();

	virtual void effect(Unit* caster, vector<Unit*> &target);
	
	string getName();
	int getMpCost();

protected:
	string mName;
	int mMpCost;
};

