#include "AssasinSkill.h"


AssasinSkill::AssasinSkill(string name, int mpCost) : SpecialSkill(name,mpCost)
{

}

AssasinSkill::~AssasinSkill()
{
}

void AssasinSkill::effect(Unit * caster, vector<Unit*>& target)
{
	int rangePercent = caster->getPow() * .2;
	int randomPow = rand() % rangePercent;
	int baseDamage = caster->getPow() * 2.2;
	int damage = 0;
	int bonusDamage;

	int compareHp = target[0]->getHp();

	

	for (int i = 0; i < target.size(); i++) //https://beginnersbook.com/2017/09/cpp-program-to-smallest-element-in-an-array/
	{
		if (compareHp > target[i]->getHp())
		{
			compareHp = target[i]->getHp();
		}
	}
	for (int i = 0; i < target.size(); i++)
	{
		if (compareHp == target[i]->getHp())
		{
			damage = (baseDamage - target[i]->getVit());
			
		
			if (target[i]->getClass() == "Mage")
			{
				bonusDamage = damage * .50;
				damage += bonusDamage;
			}

			target[i]->hpReduction(damage);
			caster->mpReduction(mMpCost);

			cout << caster->getName() << " used " << this->getName() << "to " << target[i]->getName() << "." << endl;
			cout << this->getName() << " deal " << damage << " damage to " << target[i]->getName() << endl;
		}
	}

	system("pause");
	system("CLS");	
}

string AssasinSkill::getName()
{
	return mName;
}

int AssasinSkill::getMpCost()
{
	return 0;
}
