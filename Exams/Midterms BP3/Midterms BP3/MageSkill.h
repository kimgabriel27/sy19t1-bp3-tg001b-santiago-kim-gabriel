#pragma once
#include "SpecialSkill.h"
#include <string>

using namespace std;

class MageSkill :
	public SpecialSkill
{
public:
	MageSkill(string name, int mpCost);
	~MageSkill();

	void effect(Unit* caster, vector<Unit*> &target);
	string getName();
	int getMpCost();
};

