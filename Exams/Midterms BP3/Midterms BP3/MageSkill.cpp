#include "MageSkill.h"


MageSkill::MageSkill(string name, int mpCost): SpecialSkill(name,mpCost)
{
}

MageSkill::~MageSkill()
{
}

void MageSkill::effect(Unit * caster, vector<Unit*>& target)
{
	int heal;
	int compareHp = target[0]->getHp();
	
	for (int i = 0; i < target.size(); i++) //https://beginnersbook.com/2017/09/cpp-program-to-smallest-element-in-an-array/
	{
		if (compareHp > target[i]->getHp())
		{
			compareHp = target[i]->getHp();
		}
	}
	
	for (int i = 0; i < target.size(); i++)
	{
		if (compareHp == target[i]->getHp())
		{
			heal = target[i]->getMaxHp() * .30;
			target[i]->hpGain(heal);
			caster->mpReduction(mMpCost);

			cout << caster->getName() << " used " << this->getName() << " to heal " << target[i]->getName() << endl;
			cout << this->getName() << " was healed for  " << heal << " ." <<  endl;
		}

	}

	system("pause");
	system("CLS");
}

string MageSkill::getName()
{
	return mName;
}

int MageSkill::getMpCost()
{
	return 0;
}
