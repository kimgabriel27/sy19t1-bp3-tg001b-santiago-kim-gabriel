#pragma once
#include <string>
#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;
class Skill;
class Unit
{
public:
	Unit(string name, string type, string team, int maxHp, int maxMp, int pow, int agi, int vit, int dex);
	~Unit();

	
	void chooseAction(vector<Unit*>ownTeam, vector<Unit*> enemyTeam);
	
	void randomAction(vector<Unit*>target, vector<Unit*>ownTeam);
	int hpReduction(int damage);
	int mpReduction(int mpCost);
	int hpGain(int heal);

	string getName();
	string getClass();
	string getTeam();
	int getHp();
	int getMp();
	int getMaxHp();
	int getPow();
	int getAgi();
	int getVit();
	int getDex();

	
protected:
	string mName;
	string mClass;
	string mTeam;
	int mHp;
	int mMaxHp;
	int mMp;
	int mMaxMp;
	int mPow;
	int mAgi;
	int mVit;
	int mDex;
	vector<Skill*> skills;
};

