#pragma once
#include "SpecialSkill.h"
#include <string>

using namespace std;

class WarriorSkill :
	public SpecialSkill
{
public:
	WarriorSkill(string name, int mpCost);
	~WarriorSkill();
	

	void effect(Unit* caster, vector<Unit*> &target);
	string getName();
	int getMpCost();
};

