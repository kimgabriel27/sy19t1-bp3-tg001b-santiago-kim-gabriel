#include "WarriorSkill.h"


WarriorSkill::WarriorSkill(string name, int mpCost):SpecialSkill(name,mpCost)
{
}

WarriorSkill::~WarriorSkill()
{
}

void WarriorSkill::effect(Unit * caster, vector<Unit*>& target)
{
	int size = target.size();
	
	int randomTarget = rand() % 3;

	int rangePercent = caster->getPow() * .2;
	int randomPow = rand() % rangePercent;
	int baseDamage = caster->getPow() * .9;
	int damage = (baseDamage - target[randomTarget]->getVit());
	int bonusDamage = 0;

	if (baseDamage < 0) //if baseDamage is less than 0
	{
		damage = 1;
	}

	cout << caster->getName() << " used " << this->getName() << "to all of his/her opponents." << endl;
	caster->mpReduction(this->mMpCost);

	for (int i = 0; i < size; i++)
	{
		if (target[i]->getClass() == "Assasin")
		{
			bonusDamage += damage * .50;
			target[i]->hpReduction(bonusDamage);
			cout << this->getName() << " deal " << damage << " damage to " << target[i]->getName() << endl;
		}
		else
		{
			target[i]->hpReduction(damage);
			cout << this->getName() << " deal " << damage << " damage to " << target[i]->getName() << endl;
		}
	}
	system("pause");

}

string WarriorSkill::getName()
{
	return mName;
}

int WarriorSkill::getMpCost()
{
	return mMpCost;
}
