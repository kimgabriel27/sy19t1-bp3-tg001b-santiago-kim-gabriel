#include "Skill.h"


Skill::Skill(string name, int mpCost)
{
	mName = name;
	mMpCost = mpCost;
}

Skill::~Skill()
{
}

void Skill::effect(Unit * caster, vector<Unit*>& target)
{
}

string Skill::getName()
{
	return mName;
}

int Skill::getMpCost()
{
	return mMpCost;
}
