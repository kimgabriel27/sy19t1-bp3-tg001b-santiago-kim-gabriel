#pragma once
#include "Skill.h"
#include <string>

using namespace std;

class BasicAttack :
	public Skill
{
public:
	BasicAttack(string name, int mpCost);
	~BasicAttack();
	void effect(Unit* caster, vector<Unit*>&target);
};

