#pragma once
#include "SpecialSkill.h"
#include <string>

using namespace std;

class AssasinSkill :
	public SpecialSkill
{
public:
	AssasinSkill(string name, int mpCost);
	~AssasinSkill();

	void effect(Unit* caster, vector<Unit*> &target);
	string getName();
	int getMpCost();
};

