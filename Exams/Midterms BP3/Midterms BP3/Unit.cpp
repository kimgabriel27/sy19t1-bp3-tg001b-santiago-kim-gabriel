#include "Unit.h"
#include "BasicAttack.h"
#include "WarriorSkill.h"
#include "MageSkill.h"
#include "AssasinSkill.h"
#include "Skill.h"
#include <iostream>
#include <algorithm>

Unit::Unit(string name, string type,string team, int maxHp, int maxMp, int pow, int agi, int vit, int dex)
{
	mName = name;
	mClass = type;
	mTeam = team;
	mHp = maxHp;
	mMaxHp = maxHp;
	mMaxMp = maxMp;
	mVit = vit;
	mMp = maxMp;
	mPow = pow;
	mAgi = agi;
	mDex = dex;


	if (mClass == "Warrior")
	{
		skills.push_back(new BasicAttack("Sword Breaker", 0));
		skills.push_back(new WarriorSkill("Shockwave", 5));
	}
	else if (mClass == "Mage")
	{
		skills.push_back(new BasicAttack("Staff Hit", 0));
		skills.push_back(new MageSkill("Heal", 7));
	}
	else if (mClass == "Assasin")
	{
		skills.push_back(new BasicAttack("Darkin Blade", 0));
		skills.push_back(new AssasinSkill("Assasinate", 3));
	}
}

Unit::~Unit()
{
}



void Unit::chooseAction(vector<Unit*> ownTeam , vector<Unit*>enemyTeam)
{
	int choice;
	cout << "Name: " << mName << endl;
	cout << "Class: " << mClass << endl;
	cout << "HP: " << mHp << "/" << mMaxHp << endl;
	cout << "MP: " << mMp << "/" << mMaxMp << endl;
	cout << "POW: " << mPow << endl;
	cout << "VIT: " << mVit << endl;
	cout << "DEX: " << mDex << endl;
	cout << "AGI: " << mAgi << endl << endl;	

	cout << "Choose your Move..." << endl;
	cout << "=========================================" << endl;
	cout << "[1] " << skills[0]->getName() << " (MP Cost: " << skills[0]->getMpCost() << ")" << endl;
	cout << "[2] " << skills[1]->getName() << " (MP Cost: " << skills[1]->getMpCost() << ")" << endl;
	
	cout << "Your Choice: "; cin >> choice;
	
	system("CLS");	
	//while (choice )
	//cout << "Choose your Move Again: ";
	//	cin >> choice;


	if (choice == 1)
	{
		skills[0]->effect(this, enemyTeam);
	}
	else if (choice == 2 && this->getMp() > skills[1]->getMpCost() && this->getClass() != "Mage")//not mage class (for attacking targets)
	{
		skills[1]->effect(this, enemyTeam);
	}
	else if (choice == 2 && this->getMp() > skills[1]->getMpCost() && this->getClass() == "Mage") // for mage to heal its own team.
	{
		skills[1]->effect(this, ownTeam);
	}
	

}

void Unit::randomAction(vector<Unit*> target, vector<Unit*>ownTeam)
{
	int choiceRand = rand() % 2;
	
	if (this->mMp < skills[1]->getMpCost())
	{
		skills[0]->effect(this, target);
	}
	else if (choiceRand == 2 && this->getMp() > skills[1]->getMpCost() && this->getClass() != "Mage")//not mage class (for attacking targets)
	{
		skills[1]->effect(this, target);
	}
	else if (choiceRand == 2 && this->getMp() > skills[1]->getMpCost() && this->getClass() == "Mage") // for mage to heal its own team.
	{
		skills[1]->effect(this, ownTeam);
	}
	else
	{
		skills[0]->effect(this, target);
	}


}

int Unit::hpReduction(int damage)
{
	mHp -= damage;
	return mHp;
}

int Unit::mpReduction(int mpCost)
{
	mMp -= mpCost;
	return mMp;
}

int Unit::hpGain(int heal)
{
	mHp += heal;

	if (mHp > mMaxHp)
	{
		mHp = mMaxHp;
	}
	return mHp;
}

string Unit::getName()
{
	return mName;
}

string Unit::getClass()
{
	return mClass;
}

string Unit::getTeam()
{
	return mTeam;
}

int Unit::getHp()
{
	if (mHp < 0)
	{
		mHp = 0;
	}
	return mHp;
}

int Unit::getMp()
{
	if (mMp < 0)
	{
		mMp = 0;
	}
	return mMp;
}

int Unit::getMaxHp()
{
	return mMaxHp;
}

int Unit::getPow()
{
	return mPow;
}

int Unit::getAgi()
{
	return mAgi;
}

int Unit::getVit()
{
	return mVit;
}

int Unit::getDex()
{
	return mDex;
}
