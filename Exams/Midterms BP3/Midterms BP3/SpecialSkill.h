#pragma once
#include "Skill.h"
#include <string>

using namespace std;

class SpecialSkill :
	public Skill
{
public:
	SpecialSkill(string name, int mpCost);
	~SpecialSkill();

	virtual void effect(Unit* caster, vector<Unit*>&target);
	string getName();
	int getMpCost();
};

