#include "BasicAttack.h"

BasicAttack::BasicAttack(string name, int mpCost): Skill (name,mpCost)
{
}

BasicAttack::~BasicAttack()
{
}

void BasicAttack::effect(Unit *caster, vector<Unit*>& target)
{
	int randomTarget = rand() % target.size();

	int rangePercent = caster->getPow() * .2;
	int randomPow = rand() % rangePercent;
	int baseDamage = caster->getPow() * 1.0;
	int damage = (baseDamage - target[randomTarget]->getVit());
	int bonusDamage;

	int hitRate = (caster->getDex() / target[randomTarget]->getDex()) * 100;
	int randChance = rand() % 100 + 1;


	if (hitRate < 20)
	{
		hitRate = 20;
	}

	else if (hitRate > 80)
	{
		hitRate = 80;
	}


	if (baseDamage < 0) //if baseDamage is less than 0
	{
		damage = 0;
	}


	if (randChance < hitRate)
	{
		if (caster->getClass() == "Warrior" && target[randomTarget]->getClass() == "Assasin")
		{
			bonusDamage = damage * .50;
			damage += bonusDamage;

			target[randomTarget]->hpReduction(damage);
			caster->mpReduction(this->mMpCost);

			cout << caster->getName() << " used " << this->getName() << " to " << target[randomTarget]->getName() << "." << endl;
			cout << this->getName() << " deal " << damage << " damage." << endl;

		}
		else if (caster->getClass() == "Assasin" && target[randomTarget]->getClass() == "Mage")
		{
			bonusDamage = damage * .50;
			damage += bonusDamage;

			target[randomTarget]->hpReduction(damage);
			caster->mpReduction(this->mMpCost);

			cout << caster->getName() << " used " << this->getName() << " to " << target[randomTarget]->getName() << "." << endl;
			cout << this->getName() << " deal " << damage << " damage." << endl;
		}
		else if (caster->getClass() == "Mage" && target[randomTarget]->getClass() == "Warrior")
		{
			bonusDamage = damage * .50;
			damage += bonusDamage;

			target[randomTarget]->hpReduction(damage);
			caster->mpReduction(this->mMpCost);

			cout << caster->getName() << " used " << this->getName() << " to " << target[randomTarget]->getName() << "." << endl;
			cout << this->getName() << " deal " << damage << " damage." << endl;
		}
		else
		{
			target[randomTarget]->hpReduction(damage);
			caster->mpReduction(this->mMpCost);

			cout << caster->getName() << " used " << this->getName() << " to " << target[randomTarget]->getName() << "." << endl;
			cout << this->getName() << " deal " << damage << " damage." << endl;
		}
	}

	else
	{
		cout << caster->getName() << " missed his/her attacked" << endl;
	}
	system("pause");
}
