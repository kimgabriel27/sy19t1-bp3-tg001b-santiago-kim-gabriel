/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.cpp
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
      |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/

#include "TutorialApplication.h"
#include <OgreManualObject.h>
#include "Planet.h"
#include <vector>
using namespace Ogre;

//---------------------------------------------------------------------------
TutorialApplication::TutorialApplication(void)
{
}
//---------------------------------------------------------------------------
TutorialApplication::~TutorialApplication(void)
{
}


//---------------------------------------------------------------------------
void TutorialApplication::createScene(void)
{
	Light *pointLight = mSceneMgr->createLight();
	pointLight->setType(Light::LightTypes::LT_POINT);
	pointLight->setPosition(Vector3(0, 0, 0));
	pointLight->setDiffuseColour(ColourValue(1.0f, 1.0f, 0.0f));
	pointLight->setSpecularColour(ColourValue(1.0f, 1.0f, 1.0f));
	// Values taken from: http://www.ogre3d.org/tikiwiki/-Point+Light+Attenuation
	pointLight->setAttenuation(600, 1.0, 0.007, 0.0002);
	pointLight->setCastShadows(false);

	// Lecture: Setting ambient light
	mSceneMgr->setAmbientLight(ColourValue(0.1f, 0.1f, 0.1f));
	// Create your scene here :)

	Planet *mSun = Planet::createPlanet(mSceneMgr, 20, ColourValue(1, 1, 0), "Sun");
	mSun->getNode().setPosition(0, 0, 0);
	mSun->setLocalRotationSpeed(45); 
	mSun->setRevolutionSpeed(0);


	Planet *mMercury = Planet::createPlanet(mSceneMgr, 3, ColourValue(192, 192, 192), "Mercury");
	mMercury->getNode().setPosition(36, 0, 0);
	mMercury->setLocalRotationSpeed(45);
	mMercury->setRevolutionSpeed(88);
	mMercury->setParent(mSun);

	Planet *mVenus = Planet::createPlanet(mSceneMgr, 5, ColourValue(.7, .6, .5), "Venus");
	mVenus->getNode().setPosition(67, 0, 0);
	mVenus->setLocalRotationSpeed(45);
	mVenus->setRevolutionSpeed(224);
	mVenus->setParent(mSun);


	Planet *mEarth = Planet::createPlanet(mSceneMgr, 10, ColourValue(0, 0, 1), "Earth");

	mEarth->getNode().setPosition(93, 0, 0);
	mEarth->setLocalRotationSpeed(45);
	mEarth->setRevolutionSpeed(365); 
	mEarth->setParent(mSun);

	Planet *mMars = Planet::createPlanet(mSceneMgr, 8, ColourValue(255, 165, 0), "Mars");
	mMars->getNode().setPosition(142, 0, 0);
	mMars->setLocalRotationSpeed(45);
	mMars->setRevolutionSpeed(687);
	mMars->setParent(mSun);

	Planet *mMoon = Planet::createPlanet(mSceneMgr, 1, ColourValue(192, 192, 192), "Moon");
	mMoon->getNode().setPosition(93, 0, 10);
	mMoon->setLocalRotationSpeed(45);
	mMoon->setRevolutionSpeed(200);
	mMoon->setParent(mEarth);

	planets.push_back(mSun);
	planets.push_back(mMercury);
	planets.push_back(mVenus);
	planets.push_back(mEarth);
	planets.push_back(mMars);
	planets.push_back(mMoon);

	

}


bool TutorialApplication::frameStarted(const FrameEvent & evt)
{
	for (int i = 0; i < planets.size(); i++)
	{
		planets[i]->update(evt);
		
	}
	
	return true;
}

//---------------------------------------------------------------------------

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
    INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
    int main(int argc, char *argv[])
#endif
    {
        // Create application object
        TutorialApplication app;

        try {
            app.go();
        } catch(Ogre::Exception& e)  {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
            MessageBox(NULL, e.getFullDescription().c_str(), "An exception has occurred!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
            std::cerr << "An exception has occurred: " <<
                e.getFullDescription().c_str() << std::endl;
#endif
        }

        return 0;
    }

#ifdef __cplusplus
}
#endif

//---------------------------------------------------------------------------
