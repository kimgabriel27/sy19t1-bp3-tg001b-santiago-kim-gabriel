#pragma once

#include <OgreManualObject.h>
#include <OgreSceneNode.h>
#include <OgreSceneManager.h>

using namespace Ogre;

class Planet
{
public:
	Planet(SceneNode *node);
	static Planet* createPlanet(SceneManager *sceneManager, float size, ColourValue colour, std::string name);
	~Planet();

	void update(const FrameEvent & evt);	

	SceneNode &getNode();
	void setParent(Planet *parent);	
	MaterialPtr lightSource(std::string name, ColourValue color);
	Planet *getParent();

	void setLocalRotationSpeed(float speed);
	void setRevolutionSpeed(float speed);
	static Vector3 generatePolygonNormal(Vector3 v0, Vector3 v1, Vector3 v2);
private:
	SceneNode *mNode;
	Planet *mParent;
	float mLocalRotationSpeed;
	float mRevolutionSpeed;
	
};

