#include "Planet.h"
#include "TutorialApplication.h"
#include <math.h>

using namespace Ogre;


Planet::Planet(SceneNode * node)
{
	mNode = node;
	
}

Planet * Planet::createPlanet(SceneManager *sceneManager, float size, ColourValue colour , std::string name) //
{
	ManualObject* planet = sceneManager->createManualObject();

	// NOTE: The second parameter to the create method is the resource group the material will be added to.
	// If the group you name does not exist (in your resources.cfg file) the library will assert() and your program will crash
	MaterialPtr myplanetObjectMaterial = Ogre::MaterialManager::getSingleton().create(name, "General");
	myplanetObjectMaterial->setReceiveShadows(false);
	if (name == "Sun")
	{
		myplanetObjectMaterial->getTechnique(0)->setLightingEnabled(false);
	}
	else
	{
		myplanetObjectMaterial->getTechnique(0)->setLightingEnabled(true);
	}
	myplanetObjectMaterial->getTechnique(0)->getPass(0)->setDiffuse(1,1,1,0);

	planet->begin(name, RenderOperation::OT_TRIANGLE_LIST);
	planet->colour(colour);


	// Compute for the vertices first  Source :: http://wiki.ogre3d.org/planetSphereMeshes?fbclid=IwAR2lTPon-9PlgBW-Lptydxy0gGY89f72nT6IEuOiHMW5wwfsgSz0ueedhek
	int NUM_POINTS = 8;
	float anglePerPoint = 360 / NUM_POINTS;
	float radius = size / 2;
	float latitude = (Math::PI / anglePerPoint);
	float longtitude = (2 * Math::PI / anglePerPoint);
	
	int index = 0;

	for (int i = 0; i <= anglePerPoint; i++)
	{
		float r = radius * sinf(i * latitude); // Computing the radius value 
		float y = radius * cosf(i * latitude); // Computing y value for 3D value 

		for (int j = 0; j <= anglePerPoint; j++)
		{
			float x = r * sinf(j * longtitude); // x value for 3D coordinates
			float z = r * cosf(j * longtitude); // z value for 3D coordinates
			planet->position(x, y, z); // Positioning points
			
			planet->normal(Vector3(x, y, z).normalisedCopy());
				
			if (i != anglePerPoint)  //Connecting & creationg of triangle strips 
			{
				planet->index(index + anglePerPoint + 1);
				planet->index(index);
				planet->index(index + anglePerPoint);
				planet->index(index + anglePerPoint + 1);
				planet->index(index + 1);
				planet->index(index);
				index++;
			}
		}
	}

	planet->end();

	SceneNode *createNode = sceneManager->getRootSceneNode()->createChildSceneNode();
	createNode->attachObject(planet);
		
	return new Planet(createNode);
}

Planet::~Planet()
{

}

void Planet::update(const FrameEvent & evt)
{
	//Rotation 
	Degree rotation = Degree( mLocalRotationSpeed * evt.timeSinceLastFrame);
	mNode->rotate(Vector3(0, 1, 0), Radian(rotation));

	//Revolution  https://www.gamefromscratch.com/post/2012/11/24/GameDev-math-recipes-Rotating-one-point-around-another-point.aspx
	float revolution = (mRevolutionSpeed * evt.timeSinceLastFrame) * (Math::PI/180);
	float oldX = mNode->getPosition().x;
	float oldZ = mNode->getPosition().z;

	float newX = (oldX * (Math::Cos(Radian(revolution)))) + (oldZ * (Math::Sin(Radian(revolution))));
	float newZ = (oldX * -(Math::Sin(Radian(revolution)))) + (oldZ * (Math::Cos(Radian(revolution))));
	mNode->setPosition(Vector3(newX, mNode->getPosition().y, newZ));
}

SceneNode & Planet::getNode()
{
	// TODO: insert return statement here

	return *mNode;
}


void Planet::setParent(Planet * parent)
{
	mParent = parent;

}

Planet * Planet::getParent()
{	

	return mParent;
}
  
void Planet::setLocalRotationSpeed(float speed)
{
	mLocalRotationSpeed = speed;
}
	
void Planet::setRevolutionSpeed(float speed)
{
	if (speed == 0)
	{
		mRevolutionSpeed = speed;
	}
	else
	{
		mRevolutionSpeed = (365 / speed) * 20;
	}

}

Vector3 Planet::generatePolygonNormal(Vector3 v0, Vector3 v1, Vector3 v2)
{
	
	Vector3 edge1 = v1 - v0;
	Vector3 edge2 = v2 - v0;

	Vector3 normal = edge1.crossProduct(edge2);
	normal.normalise();
	return normal;
	

}



		